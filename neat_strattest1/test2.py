import neat
import pygame
import numpy as np
import random
import os
import time
import pickle
import math

"""
mnogopotok
"""
recur = False
resume = False
restore_file = os.path.join(os.path.dirname(__file__), 'recurchkpoint-')





WIN_WIDTH = 800
WIN_HEIGHT = 800

BLUE = (0, 0, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
YELLOW = (255, 255, 0)

if not recur:
    pygame.font.init()  # init font
    SMOL_FONT = pygame.font.SysFont("comicsans", 20)
    STAT_FONT = pygame.font.SysFont("comicsans", 50)
    END_FONT = pygame.font.SysFont("comicsans", 70)
    DRAW_LINES = False
    WIN = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
    pygame.display.set_caption("neat test")

gen = 0
targets = []
tests = []

showrays = False


def getdist(point1, point2):
    return np.linalg.norm(point1 - point2)


class Worker(object):
    def __init__(self, genome, config):
        self.genome = genome
        self.config = config
        self.fitness = 0
        self.done = False

    def work(self):

        # self.env = retro.make(game="SonicTheHedgehog-Genesis", state="GreenHillZone.Act1")

        # self.env.reset()

        # ob, _, _, _ = self.env.step(self.env.action_space.sample())

        # inx = int(ob.shape[0] / 8)
        # iny = int(ob.shape[1] / 8)
        # done = False

        # net = neat.nn.FeedForwardNetwork.create(self.genome, self.config)
        net = neat.nn.recurrent.RecurrentNetwork.create(self.genome, self.config)
        serchr = Searcher(WIN_WIDTH/2, WIN_HEIGHT/2)
        # fitness = 0
        targetso = []
        while not self.done:

            if len(targetso) < 20:
                targetso.append(Food(random.random() * WIN_WIDTH, random.random() * WIN_HEIGHT))

            ai(serchr=serchr, nets=net, worker=self, targets=targetso)

            # neuroinp = []
            # neuroinp.extend(serchr.lookaround(targetso))
            # neuroinp.append(serchr.hands is None)
            # neuroinp.extend(serchr.getposes())
            #
            # output = net.activate(neuroinp)
            #
            # # print(output)
            # # output = output.clip(min=-1, max=1)
            #
            # # ge[x].fitness += 0.01 serchr.move(np.array([output[0], output[1]]))
            # serchr.move(np.array([output[0]]))
            #
            # if output[2] >= 0.5 and serchr.grab():
            #     fitness += 1
            #
            # if output[3] >= 0.5 and serchr.eat(targetso):
            #     fitness += 4
            #
            # if serchr.pos[0] < 2:
            #     serchr.pos[0] = 2
            #
            # if serchr.pos[1] < 2:
            #     serchr.pos[1] = 2
            #
            # if serchr.pos[0] > WIN_WIDTH:
            #     serchr.pos[0] = WIN_WIDTH - 2
            #
            # if serchr.pos[1] > WIN_HEIGHT:
            #     serchr.pos[1] = WIN_HEIGHT - 2
            #
            # # if serchr.pos[0] < 2 or serchr.pos[1] < 2 or serchr.pos[0] > WIN_WIDTH + 2\
            # #         or serchr.pos[1] > WIN_HEIGHT + 2 or
            #
            # if serchr.hunger <= 0 or fitness > 100:
            #     done = True
            # ge[x].fitness -= 1

        print(self.fitness)
        return self.fitness


def eval_genomes2(genome, config):
    worky = Worker(genome, config)
    return worky.work()


class Target:
    def __init__(self, x, y):
        self.pos = np.array([x, y])
        self.vel = np.array([0, 0])
        self.acc = np.array([0, 0])
        self.maxspeed = 2
        self.color = BLACK
        self.objtype = -1

class Food(Target):
    def __init__(self, x, y):
        Target.__init__(self, x, y)
        self.objtype = 2
        self.master = None

class Testblok(Target):
    def __init__(self, x, y):
        Target.__init__(self, x, y)
        self.objtype = 3




def angle_between(v1, v2):
    x = v1[0] - v2[0]
    y = v1[1] - v2[1]

    return math.atan2(x, y) - math.radians(-90)



class Searcher(Target):
    def __init__(self, x, y):
        # self.pos = np.array([x, y])
        Target.__init__(self, x, y)

        self.objtype = 1
        self.hunger = 4
        self.hands = None
        # self.collected = []
        self.target = None
        self.dirrad = 0
        # self.found = False
        
    def getdir(self):
        # re = [self.dirrad]  -self.dirrad -
        if self.target is not None:
            return self.dirrad - angle_between(self.pos, self.target.pos)
        else:
            return 0
        # if self.target is not None:
        #     return [self.pos[0] - self.target.pos[0], self.pos[1] - self.target.pos[1]]
        # else:
        #     return [0,0]
        # # print(re[1])
        # return re

    def boost(self):
        vec = angletovec(self.dirrad)
        vec = np.multiply(vec, 0.01)
        self.acc = self.acc + vec

    def deboost(self):
        self.acc = np.array([0, 0])

    def move(self, outp):
        if outp >= 0.5:
            self.boost()
        else:
            self.deboost()

        self.vel = self.vel + self.acc
        self.vel = np.clip(self.vel, -self.maxspeed, self.maxspeed)
        # print(' velc' + str(self.vel))
        self.vel = np.multiply(self.vel, 0.99)
        # print(self.vel)

        self.pos = self.pos + self.vel

        if self.hands is not None:
            self.hands.pos = self.pos

        self.hunger -= 0.005

    def grab(self):
        if self.target is not None and self.target.objtype == 2 and\
                getdist(self.pos, self.target.pos) <= 30 and self.target.master is None:
            self.hands = self.target
            self.target.master = self
            self.target = None
            return True
        else:
            return False

    def eat(self, targetso=None):
        if self.hands is not None and self.hands.objtype == 2:
            self.hunger += 4
            if not recur:
                targets.remove(self.hands)
            else:
                targetso.remove(self.hands)

            self.hands = None
            return True
        else:
            return False

    def ray(self, dir, steps, targett=None):
        stepx = 21
        rad = 0
        if dir == 0:
            rad = np.deg2rad(0)
        if dir == 1:
            rad = np.deg2rad(45)
        if dir == 2:
            rad = np.deg2rad(90)
        if dir == 3:
            rad = np.deg2rad(135)
        if dir == 4:
            rad = np.deg2rad(180)
        if dir == 5:
            rad = np.deg2rad(-135)
        if dir == 6:
            rad = np.deg2rad(-90)
        if dir == 7:
            rad = np.deg2rad(-45)
        posa = np.array([self.pos[0], self.pos[1]])
        for s in range(steps-1):
            # print(' rad is ' + str(rad))
            # print(' posa1 = ' + str(posa) + " s= " + str(s+1))
            posa = posa + (np.array(angletovec(rad))*(stepx*(s+1)))
            # print(' posa2 = ' + str(posa))
            # if showrays:
            #     tests.append(Testblok(posa[0], posa[1]))

            # print(' pos is ' + str(posa))
            etarget = checkcollisions(targett, pos=posa)
            if posa[0] < 0:
                posa[0] += WIN_WIDTH

            if posa[1] < 0:
                posa[1] += WIN_HEIGHT

            if posa[0] > WIN_WIDTH:
                posa[0] -= WIN_WIDTH

            if posa[1] > WIN_HEIGHT:
                posa[1] -= WIN_HEIGHT
            #outbound self.dirrad - rad,

            if etarget is not None and etarget != self and etarget != self.hands:
                otype = etarget.objtype

            else:
                otype = -1
                # self.target = None

            if otype > 0:
                # print(' ray colided with something in ' + str(rad))
                if otype == 2:
                    if self.target is not None and getdist(self.target.pos, self.pos) > getdist(self.pos, etarget.pos):
                        self.target = etarget
                    elif self.target is None:
                        self.target = etarget
                return [otype, getdist(self.pos, posa)]

        return [0, 0]

    def lookaround(self, target=None):
        resu = []
        for a in range(8):
            resu.extend(self.ray(a, 5, target))
        return resu

    def getposes(self):
        re = []
        re.extend(self.pos)
        if self.target is not None:
            re.extend(self.target.pos)
        else:
            re.extend([0, 0])

        return re

    def show(self):
        pygame.draw.circle(WIN, self.color, (int(self.pos[0]), int(self.pos[1])), 4)
        if showrays:
            if self.target is not None:
                textsurface = SMOL_FONT.render('m: ' + str(math.degrees(self.dirrad)) + ' t: ' +str(math.degrees(self.getdir())), False, BLACK)
                WIN.blit(textsurface, self.pos)

    def rotate(self, dir):
        self.dirrad += dir
        if self.dirrad > math.pi*2:
            self.dirrad -= math.pi*2
        elif self.dirrad < 0:
            self.dirrad += math.pi*2



def angletovec(rad):
    return [np.cos(rad), np.sin(rad)]

seethings = []


def checkcollisions(targetsu=None, who=None, pos=None):
    colbox = None

    if pos is not None:
        colbox = pygame.Rect(pos[0], pos[1], 10, 10)
    elif who is not None:
        colbox = pygame.Rect(pos[0], pos[1], 10, 10)
    if not recur:
        target = targets
    else:
        target = targetsu

    for a in target:
        whoclobx = pygame.Rect(a.pos[0], a.pos[1], 20, 20)
        if colbox.colliderect(whoclobx):
            seethings.append(a)
            return a

    return None






def savegenome(genomesa=None):
    with open(os.path.join(local_dir, "winner.pkl"), "wb") as f:
        if genomesa is not None:
            pickle.dump(genomesa, f)
        else:
            pickle.dump(populationyep.best_genome, f)
        f.close()


def savepop(populi=None):
    with open(os.path.join(local_dir, "savepop.pkl"), "wb") as f:
        pack = []
        if populi is not None:
            pack = [populi.population, populi.species, 0]
            pickle.dump(pack, f)
        else:
            pack = [populationyep.population, populationyep.species, gen]
            pickle.dump(pack, f)
        f.close()
    print(' population saved')

def loadgenome():
    with open(os.path.join(local_dir, "winner.pkl"), "rb") as f:
        return pickle.load(f)

def loadpop():
    with open(os.path.join(local_dir, "savepop.pkl"), "rb") as f:
        return pickle.load(f)

def ai(ge=None, serchr=None, nets=None, searchers=None, x=None, worker=None, targets=None):
    # serchr.lookaround()
    neuroinp = []
    # neuroinp.append(serchr.getdir())
    if worker is None:
        neuroinp.extend(serchr.lookaround())
    else:
        neuroinp.extend(serchr.lookaround(targets))

    neuroinp.append(serchr.hands is None)
    # neuroinp.extend(serchr.getposes())
    if worker is None:
        output = np.array(nets[searchers.index(serchr)].activate((neuroinp)))
    else:
        output = nets.activate(neuroinp)

    # print(output)
    # output = output.clip(min=-1, max=1)
    if worker is None:
        ge[x].fitness += 0.01
    else:
        worker.fitness += 0.01

    if output[1] >= 0.5 and serchr.grab():
        if worker is None:
            ge[x].fitness += 1
        else:
            worker.fitness += 1
    #eat
    if output[2] >= 0.5:
        if worker is None:
            if serchr.eat():
                ge[x].fitness += 4

        else:
            if serchr.eat(targets):
                worker.fitness += 4


    serchr.hunger -= 0.005
    # if output[3] >= 0.5:
    serchr.move(output[0])


    if output[3] >= 0.5:
        serchr.rotate(-0.08)
    if output[4] >= 0.5:
        serchr.rotate(0.08)

    if serchr.pos[0] < 0:
        serchr.pos[0] = WIN_WIDTH

    if serchr.pos[1] < 0:
        serchr.pos[1] = WIN_HEIGHT

    if serchr.pos[0] > WIN_WIDTH:
        serchr.pos[0] = 0

    if serchr.pos[1] > WIN_HEIGHT:
        serchr.pos[1] = 0

    # if serchr.pos[0] < 2 or serchr.pos[1] < 2 or serchr.pos[0] > WIN_WIDTH + 2\
    #         or serchr.pos[1] > WIN_HEIGHT + 2 or
    if worker is not None and worker.fitness >= 100:
        worker.done = True

    if serchr.hunger <= 0:
        # ge[x].fitness -= 1
        if worker is None:
            nets.pop(searchers.index(serchr))
            ge.pop(searchers.index(serchr))
            searchers.pop(searchers.index(serchr))
        else:
            worker.done = True


def eval_genomes(genomes, config):
    global WIN, gen, showrays
    win = WIN
    gen += 1

    nets = []
    searchers = []
    ge = []



    for genome_id, genome in genomes:
        genome.fitness = 0  # start with fitness level of 0
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        nets.append(net)
        searchers.append(Searcher(WIN_WIDTH/2, WIN_HEIGHT/2))
        ge.append(genome)

    clock = pygame.time.Clock()

    targets.clear()
    run = True

    while run and len(searchers) > 0:
        clock.tick()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()
                break
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    searchers.clear()
                if event.key == pygame.K_RETURN:
                    savepop(populationyep)
                if event.key == pygame.K_1:
                    if showrays:
                        showrays = False
                    else:
                        showrays = True

        if len(targets) < 20:
            targets.append(Food(random.random()*WIN_WIDTH, random.random()*WIN_HEIGHT))

        seethings.clear()
        tests.clear()

        for x, serchr in enumerate(searchers):
            ai(ge, serchr, nets, searchers, x)

        WIN.fill(WHITE)

        for serchr in searchers:
            serchr.show()
        for target in targets:
            size = 20
            erect = pygame.Rect(target.pos[0]-(size/2), target.pos[1]-(size/2), size, size)
            if target in seethings and target.objtype == 2:
                 pygame.draw.rect(WIN, RED, erect)
            elif target.objtype == 2:
                pygame.draw.rect(WIN, BLUE, erect)

        # if showrays:
        #     for target in tests:
        #         erect = pygame.Rect(target.pos[0], target.pos[1], 10, 10)
        #         pygame.draw.rect(WIN, YELLOW, erect)


        # for a in seethings:
        #     text = STAT_FONT.render(, True, BLACK)

        pygame.display.update()

    savepop(populationyep)


def checkforsave():
    # return os.path.isfile(os.path.join(local_dir, "winner.pkl"))
    return os.path.isfile(os.path.join(local_dir, "savepop.pkl"))


def run(config_file):
    """
    runs the NEAT algorithm to train a neural network to play flappy bird.
    :param config_file: location of config file
    :return: None
    """
    if not recur:
        config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,
                             neat.DefaultSpeciesSet, neat.DefaultStagnation,
                             config_file)

        # Create the population, which is the top-level object for a NEAT run.
        if checkforsave():
            print('   we have a save here!')
            init_pop = loadpop()
            global gen
            gen = init_pop[2]
            p = neat.Population(config, init_pop)
            # Add a stdout reporter to show progress in the terminal.

        else:
            p = neat.Population(config)

        # p.add_reporter(neat.Checkpointer(5))
        p.add_reporter(neat.StdOutReporter(True))
        stats = neat.StatisticsReporter()
        p.add_reporter(stats)
        # Run.

        global populationyep
        populationyep = p

        winner = p.run(eval_genomes, 100)



        savepop(populationyep)

        print('\nBest genome:\n{!s}'.format(winner))
    else:
        config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                             neat.DefaultSpeciesSet, neat.DefaultStagnation,
                             os.path.join(local_dir, 'config-feedforward2.txt'))

        if resume == True:
            p = neat.Checkpointer.restore_checkpoint(restore_file)
        else:
            p = neat.Population(config)
        p.add_reporter(neat.StdOutReporter(True))
        stats = neat.StatisticsReporter()
        p.add_reporter(stats)
        p.add_reporter(neat.Checkpointer(10, filename_prefix=os.path.join(local_dir, 'recurchkpoint-')))

        pe = neat.ParallelEvaluator(12, eval_genomes2)

        winner = p.run(pe.evaluate)

        with open('winner.pkl', 'wb') as output:
            pickle.dump(winner, output, 1)


if __name__ == '__main__':
    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-feedforward.txt')
    run(config_path)